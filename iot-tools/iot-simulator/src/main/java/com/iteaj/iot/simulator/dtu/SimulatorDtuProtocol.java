package com.iteaj.iot.simulator.dtu;

import com.iteaj.iot.FreeProtocolHandle;
import com.iteaj.iot.ProtocolException;
import com.iteaj.iot.ProtocolHandle;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.client.protocol.ClientProtocolCallHandle;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.simulator.SimulatorClientMessage;
import com.iteaj.iot.simulator.SimulatorProtocol;
import com.iteaj.iot.simulator.SimulatorProtocolType;

/**
 * 模拟dtu协议
 */
public class SimulatorDtuProtocol extends SimulatorProtocol {

    protected SimulatorDtuProtocol(SimulatorClientMessage message) {
        this.requestMessage = message;
    }

    public SimulatorDtuProtocol(byte[] msg, SimulatorDtuConnectProperties properties) {
        DefaultMessageHead messageHead = new DefaultMessageHead(properties.getDeviceSn(), null, protocolType());
        messageHead.setMessage(msg);
        this.setClientKey(properties);
        this.requestMessage = new SimulatorClientMessage(messageHead);
    }

    @Override
    protected SimulatorClientMessage doBuildRequestMessage() {
        return this.requestMessage();
    }

    @Override
    public void doBuildResponseMessage(SimulatorClientMessage responseMessage) { }

    @Override
    public SimulatorProtocolType protocolType() {
        return SimulatorProtocolType.DTU;
    }

    @Override
    protected ProtocolHandle getProtocolHandle() {
        // responseMessage == null 不是服务端请求的不进行业务处理
        if(this.responseMessage() != null) {
            ProtocolHandle protocolHandle = getClientKey().getProtocolHandle();
            if(protocolHandle == null) {
                protocolHandle = getDefaultProtocolHandle();
            }

            return protocolHandle;
        } else {
            return null;
        }
    }

    @Override
    public SimulatorDtuConnectProperties getClientKey() {
        return (SimulatorDtuConnectProperties) super.getClientKey();
    }

    @Override
    public <T extends ClientInitiativeProtocol> void request(ClientProtocolCallHandle handle) {
        throw new UnsupportedOperationException("不支持");
    }

    @Override
    public <T extends ClientInitiativeProtocol> void request(FreeProtocolHandle<T> handle) throws ProtocolException {
        throw new UnsupportedOperationException("不支持");
    }

    @Override
    public void request(String host, int port) {
        throw new UnsupportedOperationException("不支持");
    }

    @Override
    public void request(ClientConnectProperties server) {
        throw new UnsupportedOperationException("不支持");
    }

    @Override
    public <T extends ClientInitiativeProtocol> void request(ClientConnectProperties server, FreeProtocolHandle<T> handle) {
        throw new UnsupportedOperationException("不支持");
    }
}
