package com.iteaj.iot.server.component.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolHandle;
import com.iteaj.iot.business.ProtocolHandleFactory;
import com.iteaj.iot.server.ServerSocketProtocol;

public class DefaultSimpleChannelProtocol extends ServerSocketProtocol<DefaultSimpleServerMessage> {

    @Override
    public AbstractProtocol buildRequestMessage() {
        return null;
    }

    @Override
    public AbstractProtocol buildResponseMessage() {
        return null;
    }

    @Override
    public AbstractProtocol exec(ProtocolHandleFactory factory) {
        return null;
    }

    @Override
    public AbstractProtocol exec(ProtocolHandle handle) {
        return null;
    }

    @Override
    public <T> T protocolType() {
        return null;
    }
}
