package com.iteaj.iot.test.client.api;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.component.DelimiterBasedFrameClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import io.netty.buffer.Unpooled;

public class ApiTestClientComponent extends DelimiterBasedFrameClientComponent<ApiTestClientMessage> {

    public ApiTestClientComponent(ClientConnectProperties config) {
        super(config, 256, Unpooled.wrappedBuffer("\n".getBytes()));
    }

    @Override
    protected ServerInitiativeProtocol<ApiTestClientMessage> doGetProtocol(ApiTestClientMessage message, ProtocolType type) {
        return null;
    }

    @Override
    public String getName() {
        return "FSC Api Test";
    }

    @Override
    public String getDesc() {
        return "用于做[FrameworkManager]Api测试";
    }
}
