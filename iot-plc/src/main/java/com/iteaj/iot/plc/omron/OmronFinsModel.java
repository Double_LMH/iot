package com.iteaj.iot.plc.omron;

public enum OmronFinsModel {

    /**
     * DM区
     */
    DM((byte) 0x02, (byte) 0x82),

    /**
     * CIO区
     */
    CIO((byte)0x30, (byte)0xB0),

    /**
     * Work区
     */
    WR((byte)0x31, (byte)0xB1),

    /**
     * Holding Bit
     */
    HR((byte)0x32,  (byte)0xB2),

    /**
     * Auxiliary Bit
     */
    AR((byte)0x33,  (byte)0xB3),

    /**
     * Tim Or CNT
     */
    TIM((byte)0x09, (byte) 0x89)
    ;

    private byte bit;
    private byte word;

    OmronFinsModel(byte bit, byte word) {
        this.bit = bit;
        this.word = word;
    }

    public byte getBit() {
        return bit;
    }

    public byte getWord() {
        return word;
    }
}
