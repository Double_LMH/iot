package com.iteaj.iot.plc.siemens;

import com.iteaj.iot.Message;
import com.iteaj.iot.plc.AddressType;
import com.iteaj.iot.plc.ReadAddress;
import com.iteaj.iot.plc.WriteAddress;

import java.util.List;

public class SiemensMessageBody implements Message.MessageBody {

    private byte[] message;

    protected SiemensMessageBody(byte[] message) {
        this.message = message;
    }

    public static SiemensMessageBody buildReadBody(List<ReadAddress> batchAddress) {
        byte[] message = new byte[batchAddress.size() * 12];

        for(int i = 0; i < batchAddress.size(); i++) {
            ReadAddress address = batchAddress.get(i);
            SiemensAddressResolver resolver = SiemensAddressResolver.ParseFrom(address.getAddress(), address.getLength());

            // 位操作
            if(address.getType() == AddressType.Bit) {
                resolver.convertToBoolean();
            }

            // 指定有效值类型
            message[0 + i * 12] = 0x12;
            // 接下来本次地址访问长度
            message[1 + i * 12] = 0x0A;
            // 语法标记 ANY
            message[2 + i * 12] = 0x10;

            // 按字为单位
            if(resolver.getDataCode() == 0x1E || resolver.getDataCode() == 0x1F) {
                message[3 + i * 12] = (byte) resolver.getDataCode();
                // 访问数据的个数
                message[4 + i * 12] = (byte) (address.getLength() / 256);
                message[5 + i * 12] = (byte) (address.getLength() % 256);
            } else if(resolver.getDataCode() == 0x06 | resolver.getDataCode() == 0x07) {
                message[3 + i * 12] = 0x04;
                // 访问数据的个数
                message[4 + i * 12] = (byte) (address.getLength() / 256);
                message[5 + i * 12] = (byte) (address.getLength() % 256);
            } else {
                message[3 + i * 12] = 0x02;
                // 访问数据的个数
                message[4 + i * 12] = (byte) (address.getLength() / 256);
                message[5 + i * 12] = (byte) (address.getLength() % 256);
            }

            // 访问的是DB块
            message[6 + i * 12] = (byte) (resolver.getDbBlock() / 256);
            message[7 + i * 12] = (byte) (resolver.getDbBlock() % 256);

            // 访问数据类型
            message[8 + i * 12] = (byte) resolver.getDataCode();

            // 偏移位置
            message[9 + i * 12] = (byte) (resolver.getAddressStart() / 256 / 256 % 256);
            message[10 + i * 12] = (byte) (resolver.getAddressStart() / 256 % 256);
            message[11 + i * 12] = (byte) (resolver.getAddressStart() % 256);
        }

        return new SiemensMessageBody(message);
    }

    public static SiemensMessageBody buildWriteBody(WriteAddress address) {
        int length = address.getData().length;
        SiemensAddressResolver resolver = SiemensAddressResolver.ParseFrom(address.getAddress());

        byte[] message = new byte[16 + length];

        // 固定，返回数据长度
        message[0] = 0x12;
        message[1] = 0x0A;
        message[2] = 0x10;

        if(resolver.getDataCode() == 0x06 || resolver.getDataCode() == 0x07) {
            // 写入方式，1是按位，2是按字节，4按字
            message[3] = 0x04;

            // 写入数据的个数
            message[4] = (byte) (length / 2 / 256);
            message[5] = (byte) (length / 2 % 256);
        } else {
            // 写入方式，1是按位，2是按字节，4按字
            message[3] = 0x02;

            // 写入数据的个数
            message[4] = (byte) (length / 256);
            message[5] = (byte) (length % 256);
        }

        // 访问的是DB块
        message[6] = (byte) (resolver.getDbBlock() / 256);
        message[7] = (byte) (resolver.getDbBlock() % 256);

        // 写入数据的类型
        message[8] = (byte) resolver.getDataCode();

        // 偏移位置
        message[9] = (byte) (resolver.getAddressStart() / 256 / 256 % 256);
        message[10] = (byte) (resolver.getAddressStart() / 256 % 256);
        message[11] = (byte) (resolver.getAddressStart() % 256);

        // 写入方式
        message[12] = 0x00;
        message[13] = 0x04;

        // 计算的长度
        message[14] = (byte) (length * 8 / 256);
        message[15] = (byte) (length * 8 % 256);

        System.arraycopy(address.getData(), 0, message, 16, length);
        return new SiemensMessageBody(message);
    }

    @Override
    public byte[] getMessage() {
        return message;
    }
}
