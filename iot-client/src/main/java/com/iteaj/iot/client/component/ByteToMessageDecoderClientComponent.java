package com.iteaj.iot.client.component;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.ClientMessage;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.codec.ByteToMessageDecoderClient;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

public abstract class ByteToMessageDecoderClientComponent<M extends ClientMessage> extends TcpClientComponent<M>{

    public ByteToMessageDecoderClientComponent(ClientConnectProperties config) {
        super(config);
    }

    public ByteToMessageDecoderClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new ByteToMessageDecoderClient(this, config);
    }

    @Override
    public abstract SocketMessage doTcpDecode(ChannelHandlerContext ctx, ByteBuf decode);
}
